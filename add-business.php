<?php include 'header.php'; ?>

<section class="add-bussiness py-5">
    <div class="container-lg py-5">
        <div class="text-center">
            <h2>List Your Business For Free. Start Getting Enquires.</h2>
            <h5>Boost your search rankings for better exposure.<br>
                Matched with high-quality verified leads on WhatsApp, SMS, Notifications. <br>
                Replacement options.<br>
                Pause your ad on-demand.</h5>
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary my-btn mt-5" data-bs-toggle="modal"
                data-bs-target="#addBusinessForm">
                Let's Start
            </button>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="addBusinessForm" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
            aria-labelledby="addBusinessFormLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content h-100">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="addBusinessFormLabel">List your business</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="regForm" action="business.php"
                            class="d-flex flex-column h-100 justify-content-between">
                            <div>
                                <div class="all-steps py-4" id="all-steps">
                                    <span class="step"></span>
                                    <span class="step"></span>
                                    <span class="step"></span>
                                    <span class="step"></span>
                                    <span class="step"></span>
                                    <span class="step"></span>
                                    <span class="step"></span>
                                </div>
                                <div class="tab">
                                    <h5>Let's start with your business name</h5>
                                    <div class="tabField">
                                        <input type="text" class="form-control" placeholder="" id="busniessNameField"
                                            required>
                                        <label for="busniessNameField" class="form-label">Enter your Business
                                            Name</label>
                                    </div>
                                </div>
                                <div class="tab">
                                    <h5>Select your service category</h5>
                                    <div class="tabField">
                                        <input type="text" class="form-control" placeholder="" list="serviceCategory"
                                            id="servicecategoryField">
                                        <label for="servicecategoryField" class="form-label">Select the service your
                                            business </label>
                                        <datalist id="serviceCategory">
                                            <option value="Taxes">
                                            <option value="Real Estate">
                                            <option value="Legal">
                                            <option value="Insurance">
                                            <option value="Photography">
                                            <option value="Rental">
                                            <option value="Religious Pundits">
                                        </datalist>
                                    </div>
                                </div>
                                <div class="tab">
                                    <h5>Select the state you provide service in</h5>
                                    <div class="tabField">
                                        <input type="text" class="form-control" list="stateCategory" id="stateField"
                                            placeholder="">
                                        <label for="stateField" class="form-label">Select State</label>
                                        <datalist id="stateCategory">
                                            <option value="Alabama">
                                            <option value="Alaska">
                                            <option value="Arizona">
                                            <option value="Arkansas">
                                            <option value="California">
                                            <option value="Colorado">
                                            <option value="Connecticut">
                                            <option value="Delaware">
                                            <option value="Florida">
                                        </datalist>
                                    </div>
                                </div>
                                <div class="tab">
                                    <h5>Select the city you provide service in</h5>
                                    <div class="tabField">
                                        <input type="text" class="form-control" list="cityCategory" id="cityField"
                                            placeholder="">
                                        <label for="cityField" class="form-label">Select City</label>
                                        <datalist id="cityCategory">
                                            <option value="city 1">
                                            <option value="city 2">
                                            <option value="city 3">
                                            <option value="city 4">
                                            <option value="city 5">
                                        </datalist>
                                    </div>
                                </div>
                                <div class="tab">
                                    <h5>What your business email address?</h5>
                                    <div class="tabField">
                                        <input type="email" class="form-control" id="emailField" placeholder="">
                                        <label for="emailField" class="form-label">Enter your business email</label>
                                    </div>
                                </div>
                                <div class="tab">
                                    <h5>Your registered phone number</h5>
                                    <div class="tabField">
                                        <input type="tel" class="form-control" id="phoneField" placeholder="">
                                        <label for="phoneField" class="form-label">Enter your phone</label>
                                    </div>
                                </div>
                                <div class="tab">
                                    <div class="text-center">
                                        <p>Verify Mobile Number<br>Please enter OTP sent to <strong
                                                id="verifiedNumber">(000) 000-0000</strong>
                                        </p>
                                        <p>The OTP will expire in <span id="expire">30</span>s</p>
                                    </div>
                                    <!--
                                        <div class="tabField">
                                        <input type="number" class="form-control" id="optField" placeholder="" >
                                        <label for="optField" class="form-label">Enter OTP code</label>
                                        </div> 
                                    -->
                                    <div class="otp-fields d-flex justify-content-center py-3">
                                        <input type="number" maxlength="1" class="form-control d-inline">
                                        <input type="number" maxlength="1" class="form-control d-inline" disabled>
                                        <input type="number" maxlength="1" class="form-control d-inline" disabled>
                                        <input type="number" maxlength="1" class="form-control d-inline" disabled>
                                        <input type="number" maxlength="1" class="form-control d-inline" disabled>
                                        <input type="number" maxlength="1" class="form-control d-inline" disabled>
                                    </div>
                                    <p class="mt-3 text-center">
                                        <strong>Didn't recieve code? </strong>
                                        <a href="javascript:void(0)" id="request">Request Again!</a>
                                    </p>
                                </div>
                            </div>
                            <div class="py-2" style="overflow:auto;" id="nextprevious">
                                <div class="d-flex">
                                    <button class="btn btn-secondary w-100 me-1" type="button" id="prevBtn"
                                        onclick="nextPrev(-1)">Previous</button>
                                    <button class="btn btn-primary my-btn w-100" type="button" id="nextBtn"
                                        onclick="nextPrev(1)">Next</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php'; ?>