<?php include 'header.php'; ?>

<section class="business-profile">
    <div class="business-header">
        <div class="container-sm">
            <h1>E</h1>
        </div>
    </div>
    <div class="container-sm py-5">
        <h2 class="business-title">Elite IIT Koramangla bangalore
            <div class="btn float-end" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Edit">
                <i class="fa-regular fa-pen-to-square"></i>
            </div>
        </h2>
        <h3 class="business-address">Carrollton, Texas</h3>
        <h5 class="business-opening-hours">
            <span> </span> .
            10 AM - 7 PM . Saturday
        </h5>
        <h5 class="business-phone">
            <i class="fa-solid fa-phone-volume"></i>
            <a href="tel:098035423955">098035423955</a>
        </h5>
        <div class="py-4">
            <div class="btn ps-0">
                <span>&#9997;</span>
                Write Review
            </div>
            <div class="btn ">
                <i class="fa-solid fa-share-nodes"></i>
                Share
            </div>
        </div>

        <div class="about-sec py-3">
            <!-- Nav pills -->
            <ul class="nav about-nav">
                <li class="nav-item">
                    <a class="nav-link active" data-bs-toggle="pill" href="#aboutTab">About <span>Company</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="pill" href="#reviewTab">Reviews</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="pill" href="#socialTab">Social <span>Media</span></a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content about-content py-3">
                <div class="tab-pane active" id="aboutTab">
                    <div class="about-row">
                        <h4>Overviews</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elementum tortor non diam iaculis
                            tristique. Sed elementum feugiat enim, non blandit sapien tincidunt quis. In nec lacus
                            tellus.
                            Aenean elementum hendrerit tellus, eget commodo ipsum tempus quis. Ut eget leo mi.
                            Suspendisse
                            ullamcorper vulputate sollicitudin. Donec ut vehicula justo, ut facilisis mauris. Vestibulum
                            sollicitudin congue lacus et faucibus. Duis vehicula nisi et luctus pretium. Orci varius
                            natoque
                            penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus id viverra sem,
                            id
                            aliquam ligula. Nunc eleifend ex sed nulla bibendum dignissim. Aenean sit amet ipsum lacus.
                        </p>
                    </div>
                    <div class="about-row">
                        <h4>Services</h4>
                        <p>Real Estate</p>
                    </div>
                    <div class="about-row">
                        <h4>Serving City</h4>
                        <p>Bangalore</p>
                    </div>
                    <h4>More Information:</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="more-info">
                                <h4>Contact Person:</h4>
                                <p>Nikita</p>
                            </div>


                        </div>
                        <div class="col-md-6">
                            <div class="more-info">
                                <h4>Address:</h4>
                                <p>No.5, Krishna Nagar Layout, Koramangala, Koramangala 8th Block, Koramangala,
                                    Bengaluru, Karnataka 560030, India, Koramangala, Bangalore - 560030</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="more-info">
                                <h4>Document Verification</h4>
                                <p>
                                    <i class="fa-solid fa-suitcase"></i>
                                    Business Details -
                                    <span class="verify verified">
                                        <i class="fa-solid fa-certificate"></i>
                                        Verified
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="about-row"></div>
                    <div class="about-row"></div>
                    <div class="about-row"></div>
                    <div class="about-row"></div>
                    <div class="about-row"></div>
                </div>
                <div class="tab-pane fade" id="reviewTab">
                    <div class="row review-item">
                        <div class="col-sm-4 col-md-3 col-lg-2 text-center">
                            <div class="client-img img-hover">
                                <img src="img/experts/3.jpg" alt="Lora Lowrey" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-sm-8 col-md-9 col-lg-10 text-center text-sm-start">
                            <div class="client-text">
                                <p>I would like to thank Martin Jaramillo and his crew: Tim (flooring), Daniel (windows
                                    & doors) for an excellent job done at my newly purchased condo! I would highly
                                    recommend "Living Waters Handyman" to anyone!!!</p>
                                <h5 class="client-name">- Lora Lowrey -</h5>
                            </div>
                        </div>
                    </div>
                    <div class="row review-item">
                        <div class="col-sm-4 col-md-3 col-lg-2 text-center">
                            <div class="client-img img-hover">
                                <img src="img/experts/1.jpg" alt="Lora Lowrey" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-sm-8 col-md-9 col-lg-10 text-center text-sm-start">
                            <div class="client-text">
                                <p>I would like to thank Martin Jaramillo and his crew: Tim (flooring), Daniel (windows
                                    & doors) for an excellent job done at my newly purchased condo! I would highly
                                    recommend "Living Waters Handyman" to anyone!!!</p>
                                <h5 class="client-name">- Jemery Johnson -</h5>
                            </div>
                        </div>
                    </div>
                    <div class="row review-item">
                        <div class="col-sm-4 col-md-3 col-lg-2 text-center">
                            <div class="client-img img-hover">
                                <img src="img/experts/3.jpg" alt="Lora Lowrey" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-sm-8 col-md-9 col-lg-10 text-center text-sm-start">
                            <div class="client-text">
                                <p>I would like to thank Martin Jaramillo and his crew: Tim (flooring), Daniel (windows
                                    & doors) for an excellent job done at my newly purchased condo! I would highly
                                    recommend "Living Waters Handyman" to anyone!!!</p>
                                <h5 class="client-name">- Lora Lowrey -</h5>
                            </div>
                        </div>
                    </div>
                    <div class="row review-item">
                        <div class="col-sm-4 col-md-3 col-lg-2 text-center">
                            <div class="client-img img-hover">
                                <img src="img/experts/1.jpg" alt="Lora Lowrey" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-sm-8 col-md-9 col-lg-10 text-center text-sm-start">
                            <div class="client-text">
                                <p>I would like to thank Martin Jaramillo and his crew: Tim (flooring), Daniel (windows
                                    & doors) for an excellent job done at my newly purchased condo! I would highly
                                    recommend "Living Waters Handyman" to anyone!!!</p>
                                <h5 class="client-name">- Jemery Johnson -</h5>
                            </div>
                        </div>
                    </div>
                    <div class="row review-item">
                        <div class="col-sm-4 col-md-3 col-lg-2 text-center">
                            <div class="client-img img-hover">
                                <img src="img/experts/3.jpg" alt="Lora Lowrey" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-sm-8 col-md-9 col-lg-10 text-center text-sm-start">
                            <div class="client-text">
                                <p>I would like to thank Martin Jaramillo and his crew: Tim (flooring), Daniel (windows
                                    & doors) for an excellent job done at my newly purchased condo! I would highly
                                    recommend "Living Waters Handyman" to anyone!!!</p>
                                <h5 class="client-name">- Lora Lowrey -</h5>
                            </div>
                        </div>
                    </div>
                    <div class="row review-item">
                        <div class="col-sm-4 col-md-3 col-lg-2 text-center">
                            <div class="client-img img-hover">
                                <img src="img/experts/1.jpg" alt="Lora Lowrey" class="img-fluid">
                            </div>
                        </div>
                        <div class="col-sm-8 col-md-9 col-lg-10 text-center text-sm-start">
                            <div class="client-text">
                                <p>I would like to thank Martin Jaramillo and his crew: Tim (flooring), Daniel (windows
                                    & doors) for an excellent job done at my newly purchased condo! I would highly
                                    recommend "Living Waters Handyman" to anyone!!!</p>
                                <h5 class="client-name">- Jemery Johnson -</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="socialTab">
                    <div class="social-icon">
                        <a href="" title="Facebook" data-bs-toggle="tooltip" data-bs-placement="top"
                            data-bs-title="Facebook" target="_blank">
                            <i class="fa-brands fa-facebook-f"></i>
                        </a>
                        <a href="" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Instagram"
                            target="_blank">
                            <i class="fa-brands fa-instagram"></i>
                        </a>
                        <a href="" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Linkedin"
                            target="_blank">
                            <i class="fa-brands fa-linkedin-in"></i>
                        </a>
                        <a href="" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Twitter"
                            target="_blank">
                            <i class="fa-brands fa-twitter"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php'; ?>