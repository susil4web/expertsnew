<section class="experts-sec py-5">
    <div class="container-lg">
        <div class="sec-title">
            <h3><strong>Our Popular Experts</strong></h3>
            <p>Our service's popularity ranking is determined by a comprehensive analysis of the most frequently
                searched services and those actively fulfilled by our team of experts within a one-month timeframe. This
                ensures that our users receive recommendations based on real-time demand and the expertise of our staff,
                guaranteeing the highest level of satisfaction and relevance.</p>
        </div>

        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-6 py-3">
                <div class="expert-box">
                    <div class="expert-img">
                        <img src="img/experts/personal-taxes.jpg" alt="Personal Taxes" class="img-fluid">
                    </div>
                    <div class="expert-info p-3">
                        <h4 class="expert-name">Personal/Business Taxes</h4>
                        <div class="row align-items-center justify-content-between">
                            <div class="col py-2">
                                <p>Experts: A1234</p>
                            </div>
                            <div class="col py-2">
                                <a href="#" class="btn">Get Quates</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 py-3">
                <div class="expert-box">
                    <div class="expert-img">
                        <img src="img/experts/eb-2-processing.webp" alt="H-1B/EB2 Processing" class="img-fluid">
                    </div>
                    <div class="expert-info p-3">
                        <h4 class="expert-name">H-1B/EB2 Processing</h4>
                        <div class="row align-items-center justify-content-between">
                            <div class="col py-2">
                                <p>Experts: A1235</p>
                            </div>
                            <div class="col py-2">
                                <a href="#" class="btn">Get Quates</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 py-3">
                <div class="expert-box">
                    <div class="expert-img">
                        <img src="img/experts/real-estate-agent.jpg" alt="Real State Agent" class="img-fluid">
                    </div>
                    <div class="expert-info p-3">
                        <h4 class="expert-name">Real State Agent</h4>
                        <div class="row align-items-center justify-content-between">
                            <div class="col py-2">
                                <p>Experts: A1235</p>
                            </div>
                            <div class="col py-2">
                                <a href="#" class="btn">Get Quates</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 py-3">
                <div class="expert-box">
                    <div class="expert-img">
                        <img src="img/experts/n-400-form.jpg" alt="I-130/N-400 Form" class="img-fluid">
                    </div>
                    <div class="expert-info p-3">
                        <h4 class="expert-name">I-130/N-400 Form</h4>
                        <div class="row align-items-center justify-content-between">
                            <div class="col py-2">
                                <p>Experts: A2348</p>
                            </div>
                            <div class="col py-2">
                                <a href="#" class="btn">Get Quates</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 py-3">
                <div class="expert-box">
                    <div class="expert-img">
                        <img src="img/experts/babyshower.jpg" alt="Baby Shower Photography" class="img-fluid">
                    </div>
                    <div class="expert-info p-3">
                        <h4 class="expert-name">Baby Shower Photography</h4>
                        <div class="row align-items-center justify-content-between">
                            <div class="col py-2">
                                <p>Experts: A2348</p>
                            </div>
                            <div class="col py-2">
                                <a href="#" class="btn">Get Quates</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-4 col-sm-6 py-3">
                <div class="expert-box">
                    <div class="expert-img">
                        <img src="img/experts/employer-identification-number.jpg" alt="EIN/ITIN" class="img-fluid">
                    </div>
                    <div class="expert-info p-3">
                        <h4 class="expert-name">EIN/ITIN</h4>
                        <div class="row align-items-center justify-content-between">
                            <div class="col py-2">
                                <p>Experts: A1236</p>
                            </div>
                            <div class="col py-2">
                                <a href="#" class="btn">Get Quates</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 py-3">
                <div class="expert-box">
                    <div class="expert-img">
                        <img src="img/experts/llc-formation.jpg" alt="LLC  Formation" class="img-fluid">
                    </div>
                    <div class="expert-info p-3">
                        <h4 class="expert-name">LLC Formation</h4>
                        <div class="row align-items-center justify-content-between">
                            <div class="col py-2">
                                <p>Experts: 210</p>
                            </div>
                            <div class="col py-2">
                                <a href="#" class="btn">Get Quates</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 py-3">
                <div class="expert-box">
                    <div class="expert-img">
                        <img src="img/experts/financial-plan.webp" alt="Financial Planning" class="img-fluid">
                    </div>
                    <div class="expert-info p-3">
                        <h4 class="expert-name">Financial Planning</h4>
                        <div class="row align-items-center justify-content-between">
                            <div class="col py-2">
                                <p>Experts: 210</p>
                            </div>
                            <div class="col py-2">
                                <a href="#" class="btn">Get Quates</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>