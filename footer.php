<div class="social-share-sec">
    <div class="container-lg text-center">
        <h5>Social Share</h5>
        <div class="social-share-icons">
            <a href="">
                <i class="fa-brands fa-facebook-f"></i>
                Share on Facebook
            </a>
            <a href="">
                <i class="fa-brands fa-instagram"></i>
                Share on Instagram
            </a>
            <a href="">
                <i class="fa-brands fa-twitter"></i>
                Share on Twitter
            </a>
            <a href="">
                <i class="fa-brands fa-linkedin-in"></i>
                Share on Linkedin
            </a>
        </div>
    </div>
</div>
<!-- START : Footer Section -->
<footer>
    <div class="container-lg">
        <div class="row justify-content-between align-items-center">
            <div class="col-md-3 col-sm-4 col-5 pb-2 pb-md-0">
                <a href="index.html" class="footer-logo">
                    <img src="img/experts.jpg" alt="Experts" class="img-fluid">
                </a>
            </div>
            <div class="col-md-6 col-sm-7">
                <div class="social-icon justify-content-sm-end">
                    <a href="" title="Facebook" data-bs-toggle="tooltip" data-bs-placement="top"
                        data-bs-title="Facebook" target="_blank">
                        <i class="fa-brands fa-facebook-f"></i>
                    </a>
                    <a href="" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Instagram"
                        target="_blank">
                        <i class="fa-brands fa-instagram"></i>
                    </a>
                    <a href="" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Linkedin"
                        target="_blank">
                        <i class="fa-brands fa-linkedin-in"></i>
                    </a>
                    <a href="" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Twitter" target="_blank">
                        <i class="fa-brands fa-twitter"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="middle-footer py-4">
            <div class="row">

                <div class="col-lg-3 col-sm-6">
                    <h6>List your business</h6>
                    <ul>
                        <li>
                            <a href="#">How it works?</a>
                        </li>
                        <li>
                            <a href="#">FAQ's</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <h6>Why us?</h6>
                    <ul>
                        <li>
                            <a href="#">Community Engagment</a>
                        </li>
                        <li>
                            <a href="#">Free Marketing</a>
                        </li>
                        <li>
                            <a href="#">Increase Visiblity</a>
                        </li>
                        <li>
                            <a href="#">Customer Reviews</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <h6>Links</h6>
                    <ul>
                        <li>
                            <a href="#">About</a>
                        </li>
                        <li>
                            <a href="#">Policies</a>
                        </li>
                        <li>
                            <a href="#">Blog</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <h6>Contact Us</h6>
                    <ul>
                        <li>
                            <i class="fa-solid fa-location-dot"></i>
                            Carollton, Texas, USA
                        </li>
                        <li>
                            <i class="fa-solid fa-phone"></i>
                            +1 -
                            <a href="tel:4158184745">4158184745</a>
                        </li>
                        <li>
                            <i class="fa-solid fa-envelope"></i>
                            <a href="mailto:support@example.com">support@example.com</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="btm-footer">
            <div class="row">
                <div class="col-md-6 order-sm-1">

                </div>
                <div class="col-md-12 pt-3 pt-sm-0">
                    Copyright ©
                    <script type="text/javascript">
                    var today = new Date();
                    document.write(today.getFullYear());
                    </script> <a href="index.php">ExpertsInTown</a>. All Rights Reserved.
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- END : Footer Section -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
<script>
document.addEventListener("DOMContentLoaded", function() {
    window.addEventListener('scroll', function() {
        if (window.scrollY > 150) {
            document.getElementById('header-nav').classList.add('fixed-top');
            // add padding top to show content behind navbar
            navbar_height = document.querySelector('header').offsetHeight;
            document.body.style.paddingTop = navbar_height + 'px';
        } else {
            document.getElementById('header-nav').classList.remove('fixed-top');
            // remove padding top from body
            document.body.style.paddingTop = '0';
        }
    });
});
</script>
<?php
$url_pages = $_SERVER['REQUEST_URI'];
$ex_pages = explode("/", $url_pages);
$curr_page = $ex_pages[count($ex_pages) - 1];
 if ($curr_page == 'add-business.php') { ?>
?>
<script src="js/opt-verification.js"></script>
<?php } ?>
<script src="js/custom.js"></script>
</body>

</html>