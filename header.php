<?php
$url_pages = $_SERVER['REQUEST_URI'];
$ex_pages = explode("/", $url_pages);
$curr_page = $ex_pages[count($ex_pages) - 1];
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Experts - Discover expertise right in your town.</title>
    <link rel="icon" type="image/x-icon" href="img/experts-favicon.ico">
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.3.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css" />
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300..800;1,300..800&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100..900;1,100..900&display=swap"
        rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
</head>

<body>

    <!-- START : Header Section -->
    <header id="header-nav">
        <nav class="navbar navbar-expand-lg " data-bs-theme="dark">
            <div class="container-lg">
                <a class="navbar-brand" href="index.php">
                    <img src="img/experts.jpg" alt="Experts">
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                        <?php if ($curr_page == 'index.php' || $curr_page == '') { ?>
                        <li class="nav-item">
                            <a class="nav-link <?php echo ($curr_page == 'expertise.php') ? 'active' : ''; ?>"
                                href="#">List your expertise</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php echo ($curr_page == 'add-business.php') ? 'active' : ''; ?>"
                                href="add-business.php">Add your business</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle <?php echo ($curr_page == 'nepali.php' || $curr_page == 'indian.php' || $curr_page == 'pakistani.php' || $curr_page == 'bangaladeshi.php' || $curr_page == 'chinese.php' || $curr_page == 'korean.php' || $curr_page == 'viatnamese.php') ? 'active' : ''; ?>"
                                href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Preference
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="dropdown-item <?php echo ($curr_page == 'nepali.php') ? 'active' : ''; ?>"
                                        href="#">
                                        Nepali
                                        <img src="img/country/nepal.jpg" alt="Nepal">
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item <?php echo ($curr_page == 'indian.php') ? 'active' : ''; ?>"
                                        href="#">
                                        Indian
                                        <img src="img/country/india.jpg" alt="india">
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item <?php echo ($curr_page == 'pakistani.php') ? 'active' : ''; ?>"
                                        href="#">
                                        Pakistani
                                        <img src="img/country/pakistan.jpg" alt="Pakistani">
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item <?php echo ($curr_page == 'bangaladeshi.php') ? 'active' : ''; ?>"
                                        href="#">
                                        Bangaladeshi
                                        <img src="img/country/bangladesh.jpg" alt="Bangaladeshi">
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item <?php echo ($curr_page == 'chinese.php') ? 'active' : ''; ?>"
                                        href="#">
                                        Chinese
                                        <img src="img/country/china.jpg" alt="china">
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item <?php echo ($curr_page == 'korean.php') ? 'active' : ''; ?>"
                                        href="#">
                                        Korean
                                        <img src="img/country/korea.jpg" alt="korea">
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item <?php echo ($curr_page == 'viatnamese.php') ? 'active' : ''; ?>"
                                        href="#">
                                        Viatnamese
                                        <img src="img/country/vietnam.jpg" alt="vietnam">
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php }
                        if ($curr_page == 'add-business.php') { ?>
                        <li class="nav-item">
                            <a class="nav-link <?php echo ($curr_page == 'business-list.php') ? 'active' : ''; ?>"
                                href="#">List your business</a>
                        </li>
                        <?php }; ?>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Sign In</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <!-- END : Header Section -->