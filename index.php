<?php include 'header.php'; ?>

<!-- Start: Search Section -->
<section class="search-sec">
    <div id="searchCarousel" class="carousel slide carousel-fade" data-bs-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active" style="background-image: url('img/carousel/tax.jpg');"></div>
            <div class="carousel-item" style="background-image: url('img/carousel/immigration.jpg');"></div>
            <div class="carousel-item" style="background-image: url('img/carousel/photography.jpg');"></div>
            <div class="carousel-item" style="background-image: url('img/carousel/real-state.jpg');"></div>
            <div class="carousel-item" style="background-image: url('img/carousel/insurance.jpg');"></div>
        </div>
    </div>
    <div class="search-body">
        <div class="container-lg h-100">
            <div class="row align-items-center justify-content-center h-100">
                <div class="col-xl-9 col-md-11 col-sm-10 col-9 text-center">
                    <!-- <h2>Get connected to <strong>Local Expert.</strong></h2> -->
                    <h2><strong>FREE, EASY </strong> way to get experts.</h2>
                    <form action="search.php">
                        <div class="input-group search-form">
                            <!-- <input type="text" class="form-control" placeholder="Your text here ..."> -->
                            <select class="form-select" id="state">
                                <option selected>Select State</option>
                                <option value="">Alabama</option>
                                <option value="">Alaska</option>
                                <option value="">Arizona</option>
                                <option value="">Arkansas</option>
                                <option value="">California</option>
                                <option value="">Colorado</option>
                                <option value="">Connecticut</option>
                                <option value="">Delaware</option>
                                <option value="">Florida</option>
                            </select>
                            <select class="form-select" id="city">
                                <option selected>Select City</option>
                                <option value="">Sacramento</option>
                                <option value="">Sanfransisco</option>
                                <option value="">Los Angel</option>
                            </select>
                            <select class="form-select" id="service">
                                <option selected>Select Service</option>
                                <option value="">Taxes</option>
                                <option value="">Real Estate</option>
                                <option value="">Legal</option>
                                <option value="">Insurance</option>
                                <option value="">Photography</option>
                                <option value="">Rental</option>
                                <option value="">Religious Pundits</option>
                            </select>
                            <button class="btn btn-primary my-btn" type="submit">Search</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END: Search Section -->

<!-- START : Services Section -->
<section class="services-sec py-5">
    <div class="container-lg">
        <div class="experts-valuation py-3 ">
            <div class="row justify-content-center">
                <div class="col col-md-3 col-lg-2 text-center py-2 py-sm-0">
                    <h4>11+</h4>
                    <p>States</p>
                </div>
                <div class="col col-md-3 col-lg-2 text-center py-2 py-sm-0">
                    <h4>250+</h4>
                    <p>Verified Experts</p>
                </div>
                <div class="col col-md-3 col-lg-2 text-center py-2 py-sm-0">
                    <h4>8</h4>
                    <p>Services</p>
                </div>
            </div>
        </div>
        <div class="row justify-content-center text-center pt-3">

            <div class="col-sm-3 col-lg col-6 py-2 ">
                <a href="#" class="service-title">
                    <img src="img/service-icons/tax.png" alt="Taxes" class="img-fluid">
                    <h5>Taxes</h5>
                </a>
            </div>
            <div class="col-sm-3 col-lg col-6 py-2 ">
                <a href="#" class="service-title">
                    <img src="img/service-icons/real-estate.png" alt="Real Estate" class="img-fluid">
                    <h5>Real Estate</h5>
                </a>
            </div>
            <div class="col-sm-3 col-lg col-6 py-2 ">
                <a href="#" class="service-title">
                    <img src="img/service-icons/business.png" alt="Rental" class="img-fluid">
                    <h5>Business</h5>
                </a>
            </div>
            <div class="col-sm-3 col-lg col-6 py-2 ">
                <a href="#" class="service-title">
                    <img src="img/service-icons/insurance.png" alt="Insurance" class="img-fluid">
                    <h5>Insurance</h5>
                </a>
            </div>
            <div class="col-sm-3 col-lg col-6  py-2 ">
                <a href="#" class="service-title">
                    <img src="img/service-icons/online-form-icon.webp" alt="Rental" class="img-fluid">
                    <h5>USCIS Forms</h5>
                </a>
            </div>
            <div class="col-sm-3 col-lg col-6  py-2 ">
                <a href="#" class="service-title">
                    <img src="img/service-icons/legal.png" alt="Legal" class="img-fluid">
                    <h5>Legal</h5>
                </a>
            </div>
            <div class="col-sm-3 col-lg col-6  py-2 ">
                <a href="#" class="service-title">
                    <img src="img/service-icons/photography.png" alt="Photography" class="img-fluid">
                    <h5>Photography</h5>
                </a>
            </div>
            <div class="col-sm-3 col-lg col-6  py-2 ">
                <a href="#" class="service-title">
                    <img src="img/service-icons/pandit.png" alt="Religious Pundits" class="img-fluid">
                    <h5>Religious Pundits</h5>
                </a>
            </div>
        </div>
    </div>
</section>
<!-- END : Services Section -->

<!-- START : Best Experts Section -->
<?php include 'experts-sec.php'; ?>
<!-- END : Best Experts Section -->

<?php include 'footer.php'; ?>