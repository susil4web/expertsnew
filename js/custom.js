document.addEventListener("DOMContentLoaded", function () {
  window.addEventListener("scroll", function () {
    if (window.scrollY > 150) {
      document.getElementById("header-nav").classList.add("fixed-top");
      // add padding top to show content behind navbar
      navbar_height = document.querySelector("header").offsetHeight;
      document.body.style.paddingTop = navbar_height + "px";
    } else {
      document.getElementById("header-nav").classList.remove("fixed-top");
      // remove padding top from body
      document.body.style.paddingTop = "0";
    }
  });
});



const tooltipTriggerList = document.querySelectorAll(
  '[data-bs-toggle="tooltip"]'
);
const tooltipList = [...tooltipTriggerList].map(
  (tooltipTriggerEl) => new bootstrap.Tooltip(tooltipTriggerEl)
);

// multi step form

const now = new Date();
const hours = now.getHours();
let day = now.getDay()
let openingText = document.querySelector(".business-opening-hours span");
if (hours > 9 && hours < 17 && day != 0) {
  openingText.innerText = "Open Now";
  openingText.className = "text-primary";
}
else {
  openingText.innerText = "Closed Now";
  openingText.className = "text-danger";
}