// =========== step by step form code
var currentTab = 0;
document.addEventListener("DOMContentLoaded", function (event) {
  showTab(currentTab);
});

function showTab(n) {
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == x.length - 1) {
    document.getElementById("nextBtn").innerHTML = "Submit";
    document.getElementById("nextBtn").setAttribute("type", "submit");
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  fixStepIndicator(n);
}

function nextPrev(n) {
  var x = document.getElementsByClassName("tab");
  if (n == 1 && !validateForm()) return false;
  x[currentTab].style.display = "none";
  currentTab = currentTab + n;
  if (currentTab >= x.length) {
    document.getElementById("nextprevious").style.display = "none";
    document.getElementById("all-steps").style.display = "none";
  }
  showTab(currentTab);
}

function validateForm() {
  var x,
    y,
    i,
    valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  for (i = 0; i < y.length; i++) {
    if (y[i].value == "") {
      y[i].className += " invalid";
      y[i].setAttribute("required", "required");
      valid = false;
      debugger;
    } else {
      y[i].classList.remove("invalid");
    }
    if (y[i].type == "email") {
    } else {
    }
  }

  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid;
}

function fixStepIndicator(n) {
  var i,
    x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  // x[n].className += " active";
}







// ============== opt verification ===========

const inputs = document.querySelectorAll(".otp-fields input"),
  button = document.querySelector("button"),
  mobile = document.getElementById("mobile"),
  expire = document.getElementById("expire");

generateOPTs();
function generateOPTs() {
  inputs[0].focus();
  expire.innerText = 10;

  //   const expireInterval = setInterval(function () {
  //     expire.innerText--;
  //     if (expire.innerText == 0) {
  //       clearInterval(expireInterval);
  //     }
  //   }, 1000);
}

function clearOTPs() {}
inputs.forEach((input, index) => {
  input.addEventListener("keyup", function (e) {
    const currentIndex = input,
      nextInput = input.nextElementSibling,
      prevInput = input.previousElementSibling;
    if (
      nextInput &&
      nextInput.hasAttribute("disabled") &&
      currentIndex.value !== ""
    ) {
      nextInput.removeAttribute("disabled", true);
      nextInput.focus();
    }

    if (e.key === "Backspace") {
      inputs.forEach((input, index1) => {
        if (index <= index1 && prevInput) {
          input.setAttribute("disabled", true);
          prevInput.focus();
          prevInput.value = "";
        }
      });
      button.setAttribute("disabled", true);
    }
    if (!inputs[3].disabled && inputs[3].value !== "") {
      inputs[3].blur();
      button.removeAttribute("disabled");
    }
  });
});
