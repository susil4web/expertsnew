<?php include 'header.php'; ?>

<!-- Start: Search Section -->
<section class="search-sec" style="background-image:url('img/carousel/photography.jpg');">
    <div class="container-lg h-100">
        <div class="row align-items-center justify-content-center h-100">
            <div class="col-xl-9 col-md-11 col-sm-10 col-9 text-center">
                <div class="input-group search-form">
                    <!-- <input type="text" class="form-control" placeholder="Your text here ..."> -->
                    <select class="form-select" id="state">
                        <option selected>Choose state</option>
                        <option value="">Alabama</option>
                        <option value="">Alaska</option>
                        <option value="">Arizona</option>
                        <option value="">Arkansas</option>
                        <option value="">California</option>
                        <option value="">Colorado</option>
                        <option value="">Connecticut</option>
                        <option value="">Delaware</option>
                        <option value="">Florida</option>
                    </select>
                    <select class="form-select" id="city">
                        <option selected>Choose city ...</option>
                        <option value="">Sacramento</option>
                        <option value="">Sanfransisco</option>
                        <option value="">Los Angel</option>
                    </select>
                    <select class="form-select" id="service">
                        <option selected>Choose service...</option>
                        <option value="">Taxes</option>
                        <option value="">Real Estate</option>
                        <option value="">Legal</option>
                        <option value="">Insurance</option>
                        <option value="">Photography</option>
                        <option value="">Rental</option>
                        <option value="">Religious Pundits</option>
                    </select>
                    <button class="btn btn-primary my-btn" type="submit">Search</button>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END: Search Section -->

<!-- START : search page Section -->
<section class="search-page">
    <div class="container-lg">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb py-3">
                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                <li class="breadcrumb-item"><a href="#">California</a></li>
                <li class="breadcrumb-item"><a href="#">Sacramento</a></li>
                <li class="breadcrumb-item active" aria-current="page">Real Estate</li>
            </ol>
        </nav>


        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-9 col-xl-8">
                <div class="py-3">
                    <h4 class="text-center">Real Estate</h4>
                    <ul class="list-group list-group-horizontal justify-content-center">
                        <li class="list-group-item">Verified Experts</li>
                        <li class="list-group-item">Pocket-Friendly Rates</li>
                        <li class="list-group-item">Free Quotes</li>
                    </ul>
                    <div class="search-result row py-3 ">
                        <div class="col-3 col-sm-2 col-md-2 py-3">
                            <div class="expert-image img-hover">
                                <img src="img/experts/1.jpg" alt="Jeremy A Johnson">
                            </div>
                        </div>
                        <div class="col-sm-10 col-md-10 py-3">
                            <div class="expert-details">
                                <div class="row">
                                    <div class="col-md-8">
                                        <h3 class="expertName">1. Jeremy A Johnson, CPA</h3>
                                        <h5 class="expertStars">
                                            <span class="stars me-3">
                                                <i class="fa-solid fa-star red"></i>
                                                <i class="fa-solid fa-star red"></i>
                                                <i class="fa-solid fa-star red"></i>
                                                <i class="fa-solid fa-star red"></i>
                                                <i class="fa-solid fa-star"></i>
                                            </span>
                                            <span class="starNumber">
                                                4.0
                                            </span>
                                            <span class="reviewsNumber">
                                                (7 Reviews)
                                            </span>
                                        </h5>
                                    </div>
                                    <div class="col-md-4 text-md-end">
                                        <h6 class="expertAddress">
                                            Serving Fort Worth and the Surrounding Area
                                        </h6>
                                    </div>
                                </div>
                                <ul class="list-group list-group-horizontal-sm my-2">
                                    <li class="list-group-item">Accountants</li>
                                    <li class="list-group-item">Tax Services</li>
                                    <li class="list-group-item">Business Cunsulting</li>
                                </ul>
                                <h6>
                                    <span>
                                        <i class="fa-regular fa-id-card"></i>
                                        Certified professionals
                                    </span>
                                    <span>
                                        Locally owned & operated
                                    </span>
                                </h6>
                                <p>
                                    <i class="fa-regular fa-comment-dots"></i>
                                    "They extremely knowledgeable in <strong>tax preparation</strong> as well as
                                    budgeting and
                                    estate planning, these ..."
                                    <a href="business.php">Details</a>
                                </p>
                                <!-- <div class="text-end">
                                    <a href="" class="btn ">Request a consultation</a>
                                </div> -->
                            </div>
                        </div>
                    </div>

                    <div class="search-result row py-3 ">
                        <div class="col-3 col-sm-2 col-md-2 py-3">
                            <div class="expert-image img-hover">
                                <img src="img/experts/2.jpg" alt="Jeremy A Johnson">
                            </div>
                        </div>
                        <div class="col-sm-10 col-md-10 py-3">
                            <div class="expert-details">
                                <div class="row">
                                    <div class="col-md-8">
                                        <h3 class="expertName">2. Jeremy A Johnson, CPA</h3>
                                        <h5 class="expertStars">
                                            <span class="stars me-3">
                                                <i class="fa-solid fa-star red"></i>
                                                <i class="fa-solid fa-star red"></i>
                                                <i class="fa-solid fa-star red"></i>
                                                <i class="fa-solid fa-star red"></i>
                                                <i class="fa-solid fa-star"></i>
                                            </span>
                                            <span class="starNumber">
                                                4.0
                                            </span>
                                            <span class="reviewsNumber">
                                                (7 Reviews)
                                            </span>
                                        </h5>
                                    </div>
                                    <div class="col-md-4 text-md-end">
                                        <h6 class="expertAddress">
                                            Serving Fort Worth and the Surrounding Area
                                        </h6>
                                    </div>
                                </div>
                                <ul class="list-group list-group-horizontal-sm my-2">
                                    <li class="list-group-item">Accountants</li>
                                    <li class="list-group-item">Tax Services</li>
                                    <li class="list-group-item">Business Cunsulting</li>
                                </ul>
                                <h6>
                                    <span>
                                        <i class="fa-regular fa-id-card"></i>
                                        Certified professionals
                                    </span>
                                    <span>
                                        Locally owned & operated
                                    </span>
                                </h6>
                                <p>
                                    <i class="fa-regular fa-comment-dots"></i>
                                    "They extremely knowledgeable in <strong>tax preparation</strong> as well as
                                    budgeting and
                                    estate planning, these ..."
                                    <a href="business.php">Details</a>
                                </p>
                                <!-- <div class="text-end">
                                    <a href="" class="btn ">Request a consultation</a>
                                </div> -->
                            </div>
                        </div>
                    </div>

                    <div class="search-result row py-3 ">
                        <div class="col-3 col-sm-2 col-md-2 py-3">
                            <div class="expert-image img-hover">
                                <img src="img/experts/3.jpg" alt="Jeremy A Johnson">
                            </div>
                        </div>
                        <div class="col-sm-10 col-md-10 py-3">
                            <div class="expert-details">
                                <div class="row">
                                    <div class="col-md-8">
                                        <h3 class="expertName">3. Jeremy A Johnson, CPA</h3>
                                        <h5 class="expertStars">
                                            <span class="stars me-3">
                                                <i class="fa-solid fa-star red"></i>
                                                <i class="fa-solid fa-star red"></i>
                                                <i class="fa-solid fa-star red"></i>
                                                <i class="fa-solid fa-star red"></i>
                                                <i class="fa-solid fa-star"></i>
                                            </span>
                                            <span class="starNumber">
                                                4.0
                                            </span>
                                            <span class="reviewsNumber">
                                                (7 Reviews)
                                            </span>
                                        </h5>
                                    </div>
                                    <div class="col-md-4 text-md-end">
                                        <h6 class="expertAddress">
                                            Serving Fort Worth and the Surrounding Area
                                        </h6>
                                    </div>
                                </div>
                                <ul class="list-group list-group-horizontal-sm my-2">
                                    <li class="list-group-item">Accountants</li>
                                    <li class="list-group-item">Tax Services</li>
                                    <li class="list-group-item">Business Cunsulting</li>
                                </ul>
                                <h6>
                                    <span>
                                        <i class="fa-regular fa-id-card"></i>
                                        Certified professionals
                                    </span>
                                    <span>
                                        Locally owned & operated
                                    </span>
                                </h6>
                                <p>
                                    <i class="fa-regular fa-comment-dots"></i>
                                    "They extremely knowledgeable in <strong>tax preparation</strong> as well as
                                    budgeting and
                                    estate planning, these ..."
                                    <a href="business.php">Details</a>
                                </p>
                                <!-- <div class="text-end">
                                    <a href="" class="btn ">Request a consultation</a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="search-result row py-3 ">
                        <div class="col-3 col-sm-2 col-md-2 py-3">
                            <div class="expert-image img-hover">
                                <img src="img/experts/4.jpg" alt="Jeremy A Johnson">
                            </div>
                        </div>
                        <div class="col-sm-10 col-md-10 py-3">
                            <div class="expert-details">
                                <div class="row">
                                    <div class="col-md-8">
                                        <h3 class="expertName">4. Jeremy A Johnson, CPA</h3>
                                        <h5 class="expertStars">
                                            <span class="stars me-3">
                                                <i class="fa-solid fa-star red"></i>
                                                <i class="fa-solid fa-star red"></i>
                                                <i class="fa-solid fa-star red"></i>
                                                <i class="fa-solid fa-star red"></i>
                                                <i class="fa-solid fa-star"></i>
                                            </span>
                                            <span class="starNumber">
                                                4.0
                                            </span>
                                            <span class="reviewsNumber">
                                                (7 Reviews)
                                            </span>
                                        </h5>
                                    </div>
                                    <div class="col-md-4 text-md-end">
                                        <h6 class="expertAddress">
                                            Serving Fort Worth and the Surrounding Area
                                        </h6>
                                    </div>
                                </div>
                                <ul class="list-group list-group-horizontal-sm my-2">
                                    <li class="list-group-item">Accountants</li>
                                    <li class="list-group-item">Tax Services</li>
                                    <li class="list-group-item">Business Cunsulting</li>
                                </ul>
                                <h6>
                                    <span>
                                        <i class="fa-regular fa-id-card"></i>
                                        Certified professionals
                                    </span>
                                    <span>
                                        Locally owned & operated
                                    </span>
                                </h6>
                                <p>
                                    <i class="fa-regular fa-comment-dots"></i>
                                    "They extremely knowledgeable in <strong>tax preparation</strong> as well as
                                    budgeting and
                                    estate planning, these ..."
                                    <a href="business.php">Details</a>
                                </p>
                                <!-- <div class="text-end">
                                    <a href="" class="btn ">Request a consultation</a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




    </div>
</section>
<!-- END : search page Section -->

<!-- START : Best Experts Section -->
<?php include 'experts-sec.php'; ?>
<!-- END : Best Experts Section -->

<?php include 'footer.php'; ?>